<?php

namespace Maj\InvestmentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExpendDetail
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class ExpendDetail
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="uid", type="integer")
     */
    private $uid;

    /**
     * @var integer
     *
     * @ORM\Column(name="expenseId", type="integer")
     */
    private $expenseId;

    /**
     * @var string
     *
     * @ORM\Column(name="expenseType", type="string", length=255)
     */
    private $expenseType;

    /**
     * @var integer
     *
     * @ORM\Column(name="sourceId", type="integer")
     */
    private $sourceId;

    /**
     * @var string
     *
     * @ORM\Column(name="sourceType", type="string", length=255)
     */
    private $sourceType;

    /**
     * @var integer
     *
     * @ORM\Column(name="sourceAmount", type="integer")
     */
    private $sourceAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="sourceTotal", type="integer")
     */
    private $sourceTotal;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="isdel", type="integer", nullable=true)
     */
    private $isdel;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uid
     *
     * @param integer $uid
     *
     * @return ExpendDetail
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Get uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set expenseType
     *
     * @param string $expenseType
     *
     * @return ExpendDetail
     */
    public function setExpenseType($expenseType)
    {
        $this->expenseType = $expenseType;

        return $this;
    }

    /**
     * Get expenseType
     *
     * @return string
     */
    public function getExpenseType()
    {
        return $this->expenseType;
    }

    /**
     * Set sourceType
     *
     * @param string $sourceType
     *
     * @return ExpendDetail
     */
    public function setSourceType($sourceType)
    {
        $this->sourceType = $sourceType;

        return $this;
    }

    /**
     * Get sourceType
     *
     * @return string
     */
    public function getSourceType()
    {
        return $this->sourceType;
    }

    /**
     * Set sourceAmount
     *
     * @param integer $sourceAmount
     *
     * @return ExpendDetail
     */
    public function setSourceAmount($sourceAmount)
    {
        $this->sourceAmount = $sourceAmount;

        return $this;
    }

    /**
     * Get sourceAmount
     *
     * @return integer
     */
    public function getSourceAmount()
    {
        return $this->sourceAmount;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return ExpendDetail
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set expenseId
     *
     * @param integer $expenseId
     *
     * @return ExpendDetail
     */
    public function setExpenseId($expenseId)
    {
        $this->expenseId = $expenseId;

        return $this;
    }

    /**
     * Get expenseId
     *
     * @return integer
     */
    public function getExpenseId()
    {
        return $this->expenseId;
    }

    /**
     * Set sourceId
     *
     * @param integer $sourceId
     *
     * @return ExpendDetail
     */
    public function setSourceId($sourceId)
    {
        $this->sourceId = $sourceId;

        return $this;
    }

    /**
     * Get sourceId
     *
     * @return integer
     */
    public function getSourceId()
    {
        return $this->sourceId;
    }

    /**
     * Set sourceTotal
     *
     * @param integer $sourceTotal
     *
     * @return ExpendDetail
     */
    public function setSourceTotal($sourceTotal)
    {
        $this->sourceTotal = $sourceTotal;

        return $this;
    }

    /**
     * Get sourceTotal
     *
     * @return integer
     */
    public function getSourceTotal()
    {
        return $this->sourceTotal;
    }

    /**
     * Set isdel
     *
     * @param integer $isdel
     *
     * @return ExpendDetail
     */
    public function setIsdel($isdel)
    {
        $this->isdel = $isdel;

        return $this;
    }

    /**
     * Get isdel
     *
     * @return integer
     */
    public function getIsdel()
    {
        return $this->isdel;
    }
}
