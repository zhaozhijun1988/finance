<?php

namespace Maj\InvestmentBundle\Helper;


class ExpenseHelper
{

    private $doctrine;

    public function __construct(\Symfony\Bridge\Doctrine\RegistryInterface $doctrine)
    {
        $this->doctrine = $doctrine;
        $this->em = $this->doctrine->getManager();
    }


    public function addExpense($uid, $expenseId, $expenseType, $sourceId, $sourceType, $amount, $total, $createdAt)
    {
        $expenseDetail = new \Maj\InvestmentBundle\Entity\ExpendDetail();
        $expenseDetail->setUid($uid)
            ->setExpenseId($expenseId)
            ->setExpenseType($expenseType)
            ->setSourceId($sourceId)
            ->setSourceType($sourceType)
            ->setSourceAmount($amount)
            ->setSourceTotal($total)
            ->setCreatedAt(new \DateTime($createdAt));
        $this->em->persist($expenseDetail);
        $this->em->flush();
    }

    /*
     * 获取交易比例
     */
    public function getExpenseDetail($id, $type, $isdel = false)
    {
        $repos = $this->doctrine->getRepository('MajInvestmentBundle:ExpendDetail');
        $query = $repos->createQueryBuilder('e')
            ->select('e')
            ->where('e.expenseId=:id')
            ->andWhere('e.expenseType=:type')
            ->setParameter('id', $id)
            ->setParameter('type', $type);
        if (!$isdel)
            $query = $query->andWhere('e.isdel is null');
        return $query->getQuery()
            ->getResult();

    }

    /**
     * @param $uid
     * @param $sourceType
     * @return mixed
     */
    public function getExpenseChannel($uid, $sourceType)
    {
        $repos = $this->doctrine->getRepository('MajInvestmentBundle:ExpendDetail');
        return $repos->createQueryBuilder('e')
            ->select('e')
            ->where('e.uid=:uid')
            ->andWhere('e.sourceType=:type')
            ->andWhere('e.isdel is null')
            ->setParameter('uid', $uid)
            ->setParameter('type', $sourceType)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $uid
     * @param $sourceType
     * @return mixed
     */
    public function getExpenseChannelTotal($uid, $sourceType)
    {
        $repos = $this->doctrine->getRepository('MajInvestmentBundle:ExpendDetail');
        return $repos->createQueryBuilder('e')
            ->select('sum(e.sourceAmount)')
            ->where('e.uid=:uid')
            ->andWhere('e.sourceType=:type')
            ->andWhere('e.isdel is null')
            ->setParameter('uid', $uid)
            ->setParameter('type', $sourceType)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /*
     * 删除交易比例分配
     */
    public function deleteExpenseDetail($id, $type)
    {
        $qb = $this->em->createQuery('update \Maj\InvestmentBundle\Entity\ExpendDetail e  set e.isdel = 1 where e.expenseId=:id and
        e.expenseType=:type')->setParameter('id', $id)->setParameter('type', $type);
        $numDeleted = $qb->execute();
        return $numDeleted;
    }

    /*
     * 获取支付总额
     */
    public function getPayInfo($data)
    {
        $repos = $this->doctrine->getRepository('MajInvestmentBundle:ExpendDetail');
        foreach ($data as $key => $item) {
            $data[$key]['amount'] = $repos->createQueryBuilder('e')
                ->select('sum(e.sourceAmount)')
                ->where('e.sourceId=:id')
                ->andWhere('e.sourceType=:type')
                ->setParameter('id', $item['id'])
                ->setParameter('type', $item['type'])
                ->andWhere('e.isdel is null')
                ->getQuery()
                ->getSingleScalarResult();
        }
        return $data;
    }

    /*
     * 获取支出详细
     */
    public function getExpenseBySource($id, $type)
    {
        $repos = $this->doctrine->getRepository('MajInvestmentBundle:ExpendDetail');
        return $repos->createQueryBuilder('e')
            ->select('e')
            ->where('e.sourceId=:id')
            ->andWhere('e.sourceType=:type')
            ->setParameter('id', $id)
            ->setParameter('type', $type)
            ->andWhere('e.isdel is null')
            ->orderBy('e.createdAt', 'asc')
            ->getQuery()
            ->getResult();
    }


}