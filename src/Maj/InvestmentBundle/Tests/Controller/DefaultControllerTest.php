<?php

namespace Maj\InvestmentBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    const key = '8ca6dd2dbec5419756bab6dcd6b13fa0';

    public function testaddAction()
    {
        $client = static::createClient();
        $params = [
            [
                'uid' => 2158,
                'expenseId' => 300,
                'expenseType' => 'withdrawals',
                'sourceId' => 158,
                'sourceType' => 'investment',
                'amount' => 100,
                'total' => 1000,
                'createdAt' => date('Y-m-d')
            ]
        ];
        $crawler = $client->request('POST', '/add', [
            'data' => serialize($params),
            'sign' => md5(serialize($params) . self::key)
        ]);
        $data = json_decode($client->getResponse()->getContent(), 1);
        $this->assertEquals(0, $data['error']);
    }

    public function testQueryAction()
    {
        $client = static::createClient();
        $params = [
            'id' => 300,
            'type' => 'withdrawals',
            'isdel' => true
        ];
        $crawler = $client->request('POST', '/query', [
            'data' => serialize($params),
            'sign' => md5(serialize($params) . self::key)
        ]);

        $data = json_decode($client->getResponse()->getContent(), 1);
        $this->assertEquals(0, $data['error']);
    }

    public function testQuery2Action()
    {
        $client = static::createClient();
        $params = [
            'id' => 300,
            'type' => 'withdrawals',
            'isdel' => true
        ];
        $crawler = $client->request('POST', '/query2', [
            'data' => serialize($params),
            'sign' => md5(serialize($params) . self::key)
        ]);

        $data = json_decode($client->getResponse()->getContent(), 1);
        $this->assertEquals(0, $data['error']);
    }

    public function testpayAction()
    {
        $client = static::createClient();
        $params = [
            ['id' => 158,
                'type' => 'investment',]
        ];
        $crawler = $client->request('POST', '/pay', [
            'data' => serialize($params),
            'sign' => md5(serialize($params) . self::key)
        ]);
        $data = json_decode($client->getResponse()->getContent(), 1);
        print_r($data);
        $this->assertEquals(0, $data['error']);
    }

    public function testsourceAction()
    {
        $client = static::createClient();
        $params = [
            'id' => 158,
            'type' => 'investment'
        ];
        $crawler = $client->request('POST', '/source', [
            'data' => serialize($params),
            'sign' => md5(serialize($params) . self::key)
        ]);
        $data = json_decode($client->getResponse()->getContent(), 1);
        print_r($data);
        $this->assertEquals(0, $data['error']);
    }

    public function testwithdrawAction()
    {
        $client = static::createClient();
        $params = [
            'data'=> [158]
        ];
        $crawler = $client->request('POST', '/withdraw', [
            'data' => serialize($params),
            'sign' => md5(serialize($params) . self::key)
        ]);
        $data = json_decode($client->getResponse()->getContent(), 1);
        print_r($data);
        $this->assertEquals(0, $data['error']);
    }


    public function testdeleteAction()
    {
        $client = static::createClient();
        $params = [
            'id' => 300,
            'type' => 'withdrawals',
        ];
        $crawler = $client->request('POST', '/delete', [
            'data' => serialize($params),
            'sign' => md5(serialize($params) . self::key)
        ]);
        $data = json_decode($client->getResponse()->getContent(), 1);
        print_r($data);
        $this->assertEquals(0, $data['error']);
    }


}
