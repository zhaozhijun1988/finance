<?php

namespace Maj\InvestmentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{

    const SECRET_API = '8ca6dd2dbec5419756bab6dcd6b13fa0';


    /**
     * @Route("/add",methods="post", name="Maj_InvestmentBundle_add")
     * @Template()
     */
    public function addAction(Request $req)
    {
        $output = [];
        try {
            $data = $req->request->get('data');
            $sign = $req->request->get('sign');
            if (md5($data . self::SECRET_API) == $sign) {
                $data = unserialize($data);
                foreach ($data as $item) {
                    $this->get('maj_investment.expenseHelper')->addExpense($item['uid'], $item['expenseId'], $item['expenseType'],
                        $item['sourceId'], $item['sourceType'], $item['amount'], $item['total'], $item['createdAt']);
                }
                $output = ['error' => 0];
            } else {
                $output = [
                    'error' => 1,
                    'msg' => '验证失败'
                ];
            }

        } catch (\Exception $e) {
            $output = [
                'error' => 1,
                'msg' => $e->getMessage()
            ];
        }
        return new JsonResponse($output);
    }

    /**
     * @Route("/query",methods="post", name="Maj_InvestmentBundle_query")
     *
     */
    public function queryAction(Request $req)
    {
        $output = [];
        try {
            $data = $req->request->get('data');
            $sign = $req->request->get('sign');
            if (md5($data . self::SECRET_API) == $sign) {
                $data = unserialize($data);
                $isdel = false;
                if (array_key_exists('isdel', $data))
                    $isdel = true;
                $data = $this->get('maj_investment.expenseHelper')->getExpenseDetail($data['id'], $data['type'], $isdel);
                $result = array_map(function (\Maj\InvestmentBundle\Entity\ExpendDetail $item) {
                    return [
                        'id' => $item->getSourceId(),
                        'type' => $item->getSourceType(),
                        'amount' => $item->getSourceAmount(),
                        'total' => $item->getSourceTotal(),
                        'createdAt' => $item->getCreatedAt()->format('Y-m-d')
                    ];
                }, $data);
                $output = [
                    'error' => 0,
                    'data' => $result
                ];
            } else {
                $output = [
                    'error' => 1,
                    'data' => '验证失败'
                ];
            }

        } catch (\Exception $e) {
            $output = [
                'error' => 1,
                'msg' => $e->getMessage()
            ];
        }

        return new JsonResponse($output);
    }

    /**
     * @Route("/delete",methods="post", name="Maj_InvestmentBundle_delete")
     *
     */
    public function deleteAction(Request $req)
    {
        $output = [];
        try {
            $data = $req->request->get('data');
            $sign = $req->request->get('sign');
            if (md5($data . self::SECRET_API) == $sign) {
                $data = unserialize($data);
                $result = $this->get('maj_investment.expenseHelper')->deleteExpenseDetail($data['id'], $data['type']);
                $output = [
                    'error' => 0,
                    'data' => $result
                ];
            } else {
                $output = [
                    'error' => 1,
                    'msg' => '验证失败'
                ];
            }
        } catch (\Exception $e) {
            $output = [
                'error' => 0,
                'msg' => $e->getMessage()
            ];
        }
        return new JsonResponse($output);
    }

    /**
     * @Route("/pay",methods="post", name="Maj_InvestmentBundle_pay")
     *
     */
    public function payAction(Request $req)
    {
        $output = [];
        try {
            $data = $req->request->get('data');
            $sign = $req->request->get('sign');
            if (md5($data . self::SECRET_API) == $sign) {
                $data = unserialize($data);
                $result = $this->get('maj_investment.expenseHelper')->getPayInfo($data);
                $output = [
                    'error' => 0,
                    'data' => $result
                ];
            } else {
                $output = [
                    'error' => 1,
                    'msg' => '验证失败'
                ];
            }

        } catch (\Exception $e) {
            $output = [
                'error' => 1,
                'msg' => $e->getMessage()
            ];
        }
        return new JsonResponse($output);
    }

    /**
     * @Route("/channle/total",methods="post", name="Maj_InvestmentBundle_channel_total")
     *
     */
    public function channleTotalAction(Request $req)
    {
        $output = [];
        try {
            $data = $req->request->get('data');
            $sign = $req->request->get('sign');
            if (md5($data . self::SECRET_API) == $sign) {
                $data = unserialize($data);
                $data = $this->get('maj_investment.expenseHelper')->getExpenseChannelTotal($data['uid'], $data['sourceType']);
                $output = [
                    'error' => 0,
                    'data' => $data
                ];
            } else {
                $output = [
                    'error' => 1,
                    'msg' => '验证失败'
                ];
            }

        } catch (\Exception $e) {
            $output = [
                'error' => 1,
                'msg' => $e->getMessage()
            ];
        }
        return new JsonResponse($output);
    }

    /**
     * @Route("/channle",methods="post", name="Maj_InvestmentBundle_channel")
     *
     */
    public function channleAction(Request $req)
    {
        $output = [];
        try {
            $data = $req->request->get('data');
            $sign = $req->request->get('sign');
            if (md5($data . self::SECRET_API) == $sign) {
                $data = unserialize($data);
                $data = $this->get('maj_investment.expenseHelper')->getExpenseChannel($data['uid'], $data['sourceType']);
                $result = array_map(function (\Maj\InvestmentBundle\Entity\ExpendDetail $detail) {
                    return $detail->getSourceId();
                }, $data);
                $output = [
                    'error' => 0,
                    'data' => $result
                ];
            } else {
                $output = [
                    'error' => 1,
                    'msg' => '验证失败'
                ];
            }

        } catch (\Exception $e) {
            $output = [
                'error' => 1,
                'msg' => $e->getMessage()
            ];
        }
        return new JsonResponse($output);
    }

    /**
     * @Route("/query2",methods="post", name="Maj_InvestmentBundle_query2")
     *
     */
    public function query2Action(Request $req)
    {
        $output = [];
        try {
            $data = $req->request->get('data');
            $sign = $req->request->get('sign');
            if (md5($data . self::SECRET_API) == $sign) {
                $data = unserialize($data);
                $data = $this->get('maj_investment.expenseHelper')->getExpenseDetail($data['id'], $data['type']);
                $result = array_map(function (\Maj\InvestmentBundle\Entity\ExpendDetail $item) {
                    return [
                        'uid' => $item->getUid(),
                        'expenseId' => $item->getExpenseId(),
                        'expenseType' => $item->getExpenseType(),
                        'sourceId' => $item->getSourceId(),
                        'sourceType' => $item->getSourceType(),
                        'amount' => $item->getSourceAmount(),
                        'total' => $item->getSourceTotal(),
                        'createdAt' => $item->getCreatedAt()->format('Y-m-d')
                    ];
                }, $data);
                $output = [
                    'error' => 0,
                    'data' => $result
                ];
            } else {
                $output = [
                    'error' => 1,
                    'data' => '验证失败'
                ];
            }

        } catch (\Exception $e) {
            $output = [
                'error' => 1,
                'msg' => $e->getMessage()
            ];
        }

        return new JsonResponse($output);
    }

    /**
     * @Route("/source",methods="post", name="Maj_InvestmentBundle_source")
     *
     */
    public function sourceAction(Request $req)
    {
        $output = [];
        try {
            $data = $req->request->get('data');
            $sign = $req->request->get('sign');
            if (md5($data . self::SECRET_API) == $sign) {
                $data = unserialize($data);
                $data = $this->get('maj_investment.expenseHelper')->getExpenseBySource($data['id'], $data['type']);
                $result = array_map(function (\Maj\InvestmentBundle\Entity\ExpendDetail $item) {


                    return [
                        'expenseId' => $item->getExpenseId(),
                        'expenseType' => $item->getExpenseType(),
                        'amount' => $item->getSourceAmount(),
                        'createdAt' => $item->getCreatedAt()->format('Y-m-d')
                    ];
                }, $data);
                $output = [
                    'error' => 0,
                    'data' => $result
                ];
            } else {
                $output = [
                    'error' => 1,
                    'data' => '验证失败'
                ];
            }

        } catch (\Exception $e) {
            $output = [
                'error' => 1,
                'msg' => $e->getMessage()
            ];
        }

        return new JsonResponse($output);
    }


    /**
     * @Route("/withdraw",methods="post", name="Maj_InvestmentBundle_withdraw")
     *
     */
    public function withdrawAction(Request $req)
    {
        $output = [];
        try {
            $data = $req->request->get('data');
            $sign = $req->request->get('sign');
            if (md5($data . self::SECRET_API) == $sign) {
                $data = unserialize($data);
                $qb = $this
                    ->getDoctrine()
                    ->getRepository('MajInvestmentBundle:ExpendDetail')
                    ->createQueryBuilder('e');
                $expense = $qb->where('e.sourceId in (' . implode(',', $data['data']) . ')')
                    ->andWhere('e.sourceType=:type')
                    ->setParameter('type', 'investment')
                    ->andWhere('e.expenseType=:etype')
                    ->setParameter('etype', 'withdrawals')
                    ->andWhere('e.isdel is null')
                    ->orderBy('e.createdAt', 'desc')
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getResult();

                $output = [
                    'error' => 0,
                    'data' => [
                        'createdAt' => count($expense) > 0 ? $expense[0]->getCreatedAt()->format('Y-m-d') : ''
                    ]
                ];
            } else {
                $output = [
                    'error' => 1,
                    'data' => '验证失败'
                ];
            }

        } catch (\Exception $e) {
            $output = [
                'error' => 1,
                'msg' => $e->getMessage()
            ];
        }

        return new JsonResponse($output);
    }

    /**
     * @Route("/update",methods="post", name="Maj_InvestmentBundle_update")
     *
     */
    public function updateAction(Request $req)
    {
        $output = [];
        try {
            $data = $req->request->get('data');
            $sign = $req->request->get('sign');
            if (md5($data . self::SECRET_API) == $sign) {
                $data = unserialize($data);
                /**
                 * @var $record \Maj\InvestmentBundle\Entity\ExpendDetail
                 */
                $record = $this->getDoctrine()->getRepository('MajInvestmentBundle:ExpendDetail')->findOneBy([
                    'sourceType' => 'cashes',
                    'expenseId' => $data['expenseId'],
                    'expenseType' => $data['expenseType']
                ]);
                if ($record) {
                    $record->setSourceType($data['sourceType'])->setSourceId($data['sourceId']);
                    $this->getDoctrine()->getManager()->persist($record);
                    $this->getDoctrine()->getManager()->flush();
                    $output = [
                        'error' => 0,
                        'data' => 'success'
                    ];
                } else {
                    $output = [
                        'error' => 0,
                        'data' => '记录不存在'
                    ];
                }

            } else {
                $output = [
                    'error' => 1,
                    'data' => '验证失败'
                ];
            }

        } catch (\Exception $e) {
            $output = [
                'error' => 1,
                'msg' => $e->getMessage()
            ];
        }

        return new JsonResponse($output);
    }


}
