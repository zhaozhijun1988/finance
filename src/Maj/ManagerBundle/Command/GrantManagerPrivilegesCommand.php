<?php
namespace Maj\ManagerBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GrantManagerPrivilegesCommand  extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('grant:member:privileges')
            ->setDescription('grant member privileges')
            ->addArgument('id', InputArgument::REQUIRED, 'member id')
            ->addArgument('action', InputArgument::REQUIRED, 'action')
            ->addArgument('privileges', InputArgument::REQUIRED, 'privileges ROLE_FINANCE')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try{
            $doctrine = $this->getContainer()->get('doctrine');
            $man = $doctrine->getManager();
            $id = $input->getArgument('id');
            $action = $input->getArgument('action');
            $name = $input->getArgument('privileges');
            /**
             * @var $manager \Maj\ManagerBundle\Entity\Manager
             */
            $manager = $doctrine->getRepository('MajManagerBundle:Manager')->find($id);
            if($action == 'add'){
                $manager->setRoles(array_merge($manager->getRoles(),[$name]));
            }elseif($action == 'delete'){
                $manager->setRoles(array_diff($manager->getRoles(), [$name]));
            }elseif($action == 'clear'){
                $manager->setRoles(['ROLE_MEMBER']);
            }

            $man->persist($manager);
            $man->flush();
            $output->writeln('success');
        }catch (\Exception $e){
            $output->write($e->getMessage());
        }
    }

}
