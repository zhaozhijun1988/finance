<?php

namespace Maj\ManagerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class CrowdfundingController extends Controller
{
    /**
     * @Route("/list", methods="get", name="maj_manager_list")
     * @Template()
     */
    public function listAction()
    {

        return [];
    }

    /**
     * @Route("/export", methods="get", name="maj_manager_export")
     * @Template()
     */
    public function exportAction()
    {

        return [];
    }

}
