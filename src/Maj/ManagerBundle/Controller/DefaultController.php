<?php

namespace Maj\ManagerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;


/**
 * Class DefaultController
 * @package Maj\ManagerBundle\Controller
 * @Route("/man")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/sign-in", methods="get",name="finance_default_sign_in")
     * @Template()
     */
    public function signInAction(Request $req)
    {
        $session = $req->getSession();

        if ($req->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $req->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        }
        else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return ['last_username' => $session->get(SecurityContext::LAST_USERNAME),
            'error'         => $error];
    }


    /**
     * @Route("/login-out", name="finance_default_login_out")
     */
    public function loginOutAction()
    {
       return [];
    }
}
