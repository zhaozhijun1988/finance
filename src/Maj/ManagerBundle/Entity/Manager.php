<?php

namespace Maj\ManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Manager
 *
 * @ORM\Table()
 * @ORM\Entity
 * @UniqueEntity("uid")
 */
class Manager implements UserInterface, \Serializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="uid", type="integer")
     */
    private $uid;

    /**
     * @var array
     *
     * @ORM\Column(name="roles", type="json_array", nullable=true)
     */
    private $roles;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255, nullable=true)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=255, nullable=true)
     */
    private $salt;

    /**
     * @var \Mojomaja\Component\Aserv\User
     */
    private $user;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uid
     *
     * @param integer $uid
     *
     * @return Manager
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Get uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set roles
     *
     * @param array $roles
     *
     * @return Manager
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Get roles
     *
     * @return array
     */
    public function getRoles()
    {
        if(!$this->roles)
            return $roles = ['ROLE_MEMBER'];
        return $this->roles;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return Manager
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        if ($this->user)
            return current(array_filter([
                $this->user->getName(),
                $this->user->getMobile() ? substr_replace($this->user->getMobile(), '****', 7, 4 ): '',
                $this->user->getEmail()
            ]));

        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return Manager
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set salt
     *
     * @param string $salt
     *
     * @return Manager
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    public function serialize()
    {
        return serialize(array($this->id, $this->user->getId(), $this->getUsername()));
    }

    public function unserialize($serialized)
    {
        list($this->id, $u, $this->username) = unserialize($serialized);
        $this->user = new \Mojomaja\Component\Aserv\User(['id' => $u]);
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function setUser(\Mojomaja\Component\Aserv\User $user)
    {
        $this->user = $user;
        $this->setUid($user->getId());

        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function bringUp()
    {
        $this
            ->setUid($this->getUser()->getId())

        ;

        return $this;
    }


}
