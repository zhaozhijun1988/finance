<?php
namespace Maj\ManagerBundle\Security;

use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class RoleSuccessHandler extends DefaultAuthenticationSuccessHandler
{
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        if($this->security->isGranted('ROLE_FINANCE')){
            $target_url = 'maj_manager_list';
        }else{
            throw new \Exception('未授权用户');
        }

        return $this->httpUtils->createRedirectResponse($request, $target_url);
    }

    public function setSecurityContext($securityContext)
    {
        $this->security = $securityContext;
    }


}