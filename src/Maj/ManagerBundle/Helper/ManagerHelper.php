<?php

namespace Maj\ManagerBundle\Helper;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Util\SecureRandom;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Mojomaja\Bundle\AservBundle\Aid\UserAidInterface;
use Mojomaja\Bundle\AservBundle\Aid\AservAid;
use Mojomaja\Component\Aserv\User;
use Mojomaja\Component\Aserv\Client;
use Maj\ManagerBundle\Entity\Manager;

class ManagerHelper implements UserAidInterface
{
    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $em;

    /**
     * @var \Mojomaja\Bundle\AservBundle\Aid\AservAid
     */
    private $aserv;


    public function __construct(Registry $doctrine, AservAid $aserv, Client $client)
    {
        $this->em = $doctrine->getManager();
        $this->aserv = $aserv;
        $this->client = $client;
    }

    public function emerge(User $user)
    {
        $manager =
            $this
                ->em
                ->getRepository('MajManagerBundle:Manager')
                ->findOneBy(['uid' => $user->getId()]);
        if (!$manager) {
            $manager = new \Maj\ManagerBundle\Entity\Manager();
            $manager
                ->setPassword(base64_encode((new SecureRandom())->nextBytes(18)))
                ->setUser($user)
                ->bringUp();

            $this->em->persist($manager);
            $this->em->flush($manager);

        }

        return $manager->setUser($user);
    }


    public function registerMember($mobile, $password)
    {
        $user = $this->client->register(
            new \Mojomaja\Component\Aserv\User([
                'mobile' => $mobile
            ]),
            $password
        );
        $manager =
            $this
                ->em
                ->getRepository('MajManagerBundle:Manager')
                ->findOneBy(['uid' => $user->getId()]);
        if ($manager)
            return $manager->setUser($user);
        $member = new \Maj\ManagerBundle\Entity\Manager();
        $member->setUid($user->getId());
        $member->setUser($user);
        //  $member->setCreatedAt(new \DateTime());
        $this->em->persist($member);
        $this->em->flush();
        return $member;
    }


    public function refresh(UserInterface $user)
    {
        $manager = $this->em->find('MajManagerBundle:Manager', $user->getId());
        if (!$manager)
            throw new UsernameNotFoundException(sprintf('User with id %s not found', json_encode($user->getId())));

        return $manager->setUser($this->aserv->retrieve($user->getUser()->getId()));
    }

    public function boost(array $manager, $active = true)
    {
        if (!($manager = array_filter($manager)))
            return;

        $user = $this->client->fetch(array_map(function (Manager $m) {
            return $m->getUid();
        }, $manager), $active);
        $user = array_combine(array_map(function (User $u) {
            return $u->getId();
        }, $user), $user);
        foreach ($manager as $m)
            $m->setUser(
                array_key_exists($m->getUid(), $user) ?
                    $user[$m->getUid()] :
                    new User()
            );
    }


}