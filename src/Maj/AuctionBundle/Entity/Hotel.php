<?php

namespace Maj\AuctionBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hotel
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Hotel
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="item_number", type="string", length=255)
     */
    private $itemNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="property_zip", type="string", length=255)
     */
    private $propertyZip;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="lot_size", type="string", length=255, nullable=true)
     */
    private $lotSize;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="occupancy_rate", type="string", length=255, nullable=true)
     */
    private $occupancyRate;

    /**
     * @var string
     *
     * @ORM\Column(name="property_type", type="string", length=255)
     */
    private $propertyType;

    /**
     * @var integer
     *
     * @ORM\Column(name="property_id", type="integer")
     */
    private $propertyId;

    /**
     * @var integer
     *
     * @ORM\Column(name="start_bid", type="integer", nullable=true)
     */
    private $start_bid;

    /**
     * @var integer
     *
     * @ORM\Column(name="end_bid", type="integer", nullable=true)
     */
    private $end_bid;


    /**
     * @var string
     *
     * @ORM\Column(name="auction_number", type="string", length=255)
     */
    private $auctionNumber;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime")
     */
    private $endDate;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=255)
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(name="type_original", type="string", length=255)
     */
    private $typeOriginal;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="thumb", type="string", length=255)
     */
    private $thumb;

    /**
     * @var string
     *
     * @ORM\Column(name="auction_name", type="string", length=255)
     */
    private $auctionName;

    /**
     * @var string
     *
     * @ORM\Column(name="units_number", type="string", length=255, nullable=true)
     */
    private $units_number;

    /**
     * @var string
     *
     * @ORM\Column(name="net_rentable_area", type="string", length=255, nullable=true)
     */
    private $net_rentable_area;

    /**
     * @var string
     *
     * @ORM\Column(name="auction_status", type="string", length=255, nullable=true)
     */
    private $auctionStatus;


    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="md5url", type="string", length=255)
     */
    private $md5url;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isend", type="boolean", nullable=true)
     */
    private $isend;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set itemNumber
     *
     * @param string $itemNumber
     *
     * @return Hotel
     */
    public function setItemNumber($itemNumber)
    {
        $this->itemNumber = $itemNumber;

        return $this;
    }

    /**
     * Get itemNumber
     *
     * @return string
     */
    public function getItemNumber()
    {
        return $this->itemNumber;
    }

    /**
     * Set propertyZip
     *
     * @param string $propertyZip
     *
     * @return Hotel
     */
    public function setPropertyZip($propertyZip)
    {
        $this->propertyZip = $propertyZip;

        return $this;
    }

    /**
     * Get propertyZip
     *
     * @return string
     */
    public function getPropertyZip()
    {
        return $this->propertyZip;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Hotel
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set lotSize
     *
     * @param string $lotSize
     *
     * @return Hotel
     */
    public function setLotSize($lotSize)
    {
        $this->lotSize = $lotSize;

        return $this;
    }

    /**
     * Get lotSize
     *
     * @return string
     */
    public function getLotSize()
    {
        return $this->lotSize;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Hotel
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set occupancyRate
     *
     * @param string $occupancyRate
     *
     * @return Hotel
     */
    public function setOccupancyRate($occupancyRate)
    {
        $this->occupancyRate = $occupancyRate;

        return $this;
    }

    /**
     * Get occupancyRate
     *
     * @return string
     */
    public function getOccupancyRate()
    {
        return $this->occupancyRate;
    }

    /**
     * Set propertyType
     *
     * @param string $propertyType
     *
     * @return Hotel
     */
    public function setPropertyType($propertyType)
    {
        $this->propertyType = $propertyType;

        return $this;
    }

    /**
     * Get propertyType
     *
     * @return string
     */
    public function getPropertyType()
    {
        return $this->propertyType;
    }

    /**
     * Set propertyId
     *
     * @param integer $propertyId
     *
     * @return Hotel
     */
    public function setPropertyId($propertyId)
    {
        $this->propertyId = $propertyId;

        return $this;
    }

    /**
     * Get propertyId
     *
     * @return integer
     */
    public function getPropertyId()
    {
        return $this->propertyId;
    }

    /**
     * Set auctionNumber
     *
     * @param string $auctionNumber
     *
     * @return Hotel
     */
    public function setAuctionNumber($auctionNumber)
    {
        $this->auctionNumber = $auctionNumber;

        return $this;
    }

    /**
     * Get auctionNumber
     *
     * @return string
     */
    public function getAuctionNumber()
    {
        return $this->auctionNumber;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return Hotel
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return Hotel
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set typeOriginal
     *
     * @param string $typeOriginal
     *
     * @return Hotel
     */
    public function setTypeOriginal($typeOriginal)
    {
        $this->typeOriginal = $typeOriginal;

        return $this;
    }

    /**
     * Get typeOriginal
     *
     * @return string
     */
    public function getTypeOriginal()
    {
        return $this->typeOriginal;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Hotel
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set auctionName
     *
     * @param string $auctionName
     *
     * @return Hotel
     */
    public function setAuctionName($auctionName)
    {
        $this->auctionName = $auctionName;

        return $this;
    }

    /**
     * Get auctionName
     *
     * @return string
     */
    public function getAuctionName()
    {
        return $this->auctionName;
    }

    /**
     * Set isend
     *
     * @param boolean $isend
     *
     * @return Hotel
     */
    public function setIsend($isend)
    {
        $this->isend = $isend;

        return $this;
    }

    /**
     * Get isend
     *
     * @return boolean
     */
    public function getIsend()
    {
        return $this->isend;
    }

    /**
     * Set thumb
     *
     * @param string $thumb
     *
     * @return Hotel
     */
    public function setThumb($thumb)
    {
        $this->thumb = $thumb;

        return $this;
    }

    /**
     * Get thumb
     *
     * @return string
     */
    public function getThumb()
    {
        return $this->thumb;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Hotel
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set md5url
     *
     * @param string $md5url
     *
     * @return Hotel
     */
    public function setMd5url($md5url)
    {
        $this->md5url = $md5url;

        return $this;
    }

    /**
     * Get md5url
     *
     * @return string
     */
    public function getMd5url()
    {
        return $this->md5url;
    }

    /**
     * Set startBid
     *
     * @param integer $startBid
     *
     * @return Hotel
     */
    public function setStartBid($startBid)
    {
        $this->start_bid = $startBid;

        return $this;
    }

    /**
     * Get startBid
     *
     * @return integer
     */
    public function getStartBid()
    {
        return $this->start_bid;
    }

    /**
     * Set endBid
     *
     * @param integer $endBid
     *
     * @return Hotel
     */
    public function setEndBid($endBid)
    {
        $this->end_bid = $endBid;

        return $this;
    }

    /**
     * Get endBid
     *
     * @return integer
     */
    public function getEndBid()
    {
        return $this->end_bid;
    }

    /**
     * Set unitsNumber
     *
     * @param string $unitsNumber
     *
     * @return Hotel
     */
    public function setUnitsNumber($unitsNumber)
    {
        $this->units_number = $unitsNumber;

        return $this;
    }

    /**
     * Get unitsNumber
     *
     * @return string
     */
    public function getUnitsNumber()
    {
        return $this->units_number;
    }

    /**
     * Set netRentableArea
     *
     * @param string $netRentableArea
     *
     * @return Hotel
     */
    public function setNetRentableArea($netRentableArea)
    {
        $this->net_rentable_area = $netRentableArea;

        return $this;
    }

    /**
     * Get netRentableArea
     *
     * @return string
     */
    public function getNetRentableArea()
    {
        return $this->net_rentable_area;
    }

    /**
     * Set auctionStatus
     *
     * @param string $auctionStatus
     *
     * @return Hotel
     */
    public function setAuctionStatus($auctionStatus)
    {
        $this->auctionStatus = $auctionStatus;

        return $this;
    }

    /**
     * Get auctionStatus
     *
     * @return string
     */
    public function getAuctionStatus()
    {
        return $this->auctionStatus;
    }
}
