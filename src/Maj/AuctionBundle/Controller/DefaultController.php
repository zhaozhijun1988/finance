<?php

namespace Maj\AuctionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Tools\Pagination\Paginator;

class DefaultController extends Controller
{
    const PER_PAGE = 30;

    /**
     * @Route("/list/{page}",methods="get", name="Maj_AuctionBundle_index", defaults={"page"="1"}, requirements={"page"="\d+"})
     * @Template()
     */
    public function indexAction($page)
    {
        $doctrine = $this->getDoctrine()->getRepository('MajAuctionBundle:Hotel');
        $qb = $doctrine->createQueryBuilder('h')
            ->orderBy('h.id', 'asc')
            //  ->setFirstResult(($page - 1) * self::PER_PAGE)
            //->setMaxResults(self::PER_PAGE)
            ->getQuery();
        $data = new Paginator($qb);

        return [
            'data' => $data,
            'current' => $page,
            'total' => ceil(count($data) / self::PER_PAGE)
        ];
    }


    /**
     * @Route("/detail/{id}",methods="get", name="Maj_AuctionBundle_detail", requirements={"page"="\d+"})
     * @Template()
     */
    public function detailAction($id)
    {
        $repos = $this->getDoctrine()->getRepository('MajAuctionBundle:Hotel');
        $hotel = $repos->find($id);
        return [
            'hotel' => $hotel
        ];
    }

}
