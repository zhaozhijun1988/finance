<?php

namespace Maj\AuctionBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class ActionSpiderCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('auction:spider:start')
            ->setDescription('auction spider data');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $domain = 'https://www.auction.com/commercial/0_row/';
            $content = $this->getContainer()->get('maj_auction.skurl')->get($domain);
            $temp = explode('ADC.model.search = ', $content);
            $temp = explode(';ADC.model.feature_flags', $temp[1]);
            $data = json_decode($temp[0], 1);
            $doctrine = $this->getContainer()->get('doctrine');
            $repos = $doctrine->getRepository('MajAuctionBundle:Hotel');
            echo count($data['asset']);
            foreach ($data['asset'] as $key => $item) {
                $output->writeln($key);
                $hotel = $repos->findOneBy(['md5url' => md5($item['pdp_url'])]);
                if (!$hotel) {
                    $hotel = new \Maj\AuctionBundle\Entity\Hotel();
                    $hotel->setAuctionName($item['auction_name'])
                        ->setAuctionNumber($item['auction_number'])
                        ->setDescription($item['property_description'])
                        ->setEndDate(new \DateTime($item['auction_end_date_utc']))
                        ->setItemNumber($item['item_number'])
                        ->setNetRentableArea(array_key_exists('net_rentable_area', $item) ? $item['net_rentable_area'] : '')
                        ->setType($item['auction_type'])
                        ->setLotSize(array_key_exists('lot_size', $item) ? $item['lot_size'] : '')
                        ->setTypeOriginal($item['product_type_original'])
                        ->setName($item['property_name'])
                        ->setOccupancyRate(array_key_exists('occupancy_rate', $item) ? $item['occupancy_rate'] : '')
                        ->setPropertyType($item['property_type'])
                        ->setPropertyId($item['global_property_id'])
                        ->setUnitsNumber(array_key_exists('number_of_units', $item) ? $item['number_of_units'] : 0)
                        ->setPropertyZip($item['property_zip'])
                        ->setMd5url(md5($item['pdp_url']))
                        ->setState($item['property_state'])
                        ->setStartBid(array_key_exists('starting_bid', $item) ? intval($item['starting_bid']) : 0)
                        ->setThumb($item['thumbnail'])
                        ->setUrl($item['pdp_url']);
                    $doctrine->getManager()->persist($hotel);
                    $doctrine->getManager()->flush();
                    $output->writeln($hotel->getId());
                }
            }

        } catch (\Exception $e) {
            echo $e->getMessage();
        }


    }

} 