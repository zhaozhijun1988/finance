<?php


namespace Maj\AuctionBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class BidCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('auction:bid:start')
            ->setDescription('auction spider bid data');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $skurl = $this->getContainer()->get('maj_auction.skurl');
        $repos = $doctrine->getRepository('MajAuctionBundle:Hotel');
        $result = $repos->createQueryBuilder('h')
            ->select('h')
            ->where('h.endDate <=:date')
            ->setParameter('date', new \DateTime('+2 days'))
            ->andWhere('h.isend is null')
            ->orderBy('h.id', 'asc')
            ->getQuery()
            ->getResult();
        $result = array_map(function (\Maj\AuctionBundle\Entity\Hotel $hotel) use ($doctrine, $skurl) {
            $data = $skurl->post('https://www.auction.com/bidding/status/',
                [
                    'auction_number' => $hotel->getAuctionNumber(),
                    'gpid' => $hotel->getPropertyId()
                ]);
            $temp = json_decode($data, 1);
            if (array_key_exists('error', $temp)) {
                $hotel->setIsend(true);
            } else {
                $hotel->setAuctionStatus($temp['auction_status']);
                if ($temp['auction_status'] == 'Sold' || $temp['auction_status'] == 'Gone') {
                    $hotel->setEndBid($temp['current_bid_amount']);
                    $hotel->setIsend(true);
                } else
                    $hotel->setEndBid($temp['current_bid_amount']);

            }
            $doctrine->getManager()->persist($hotel);
            $doctrine->getManager()->flush();
            sleep(10);
            echo $hotel->getId() . "\r\n";
        }, $result);
    }

} 